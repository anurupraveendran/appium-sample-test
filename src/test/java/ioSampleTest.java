import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.Keys;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class ioSampleTest {

    public AppiumDriver<MobileElement> driver;
    public WebDriverWait wait;

    //Elements
    //String secondNewJob = "//android.widget.FrameLayout[@index=0]/android.support.v4.widget.DrawerLayout[@index=0]/android.widget.LinearLayout[@index=0]/android.widget.ImageButton[@index=0]";
    String hamburgerButtonXP = "//android.widget.ImageButton";
    String signInButtonXP = "//android.widget.TextView[@index=0]";
    String signUpButtonXP = "//android.widget.TextView[@index='1'][@text='Sign Up']";
    String firstNameTBoxXP = "//android.widget.EditText[@text='First Name']";
    String lastNameTBoxXP = "//android.widget.EditText[@text='Last Name']";
    String emailTBoxXP = "//android.widget.EditText[@text='Email']";
    String passwordTBoxXP = "//android.widget.EditText[@text='Password']";
    String zipCodeTBoxXP = "//android.widget.EditText[@text='Zip Code']";
    String submitButtonXP = "//android.widget.Button[@text='Create Account']";
    //String firstNameTBoxXP = "//android.widget.EditText[@index='0'][@text='First Name']";
    //String signInButtonXP = "//android.widget.CheckedTextView";

    @BeforeMethod
    public void setup () throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("deviceName", "Galaxy S2");
        caps.setCapability("udid", "emulator-5554"); //DeviceId from "adb devices" command
        caps.setCapability("platformName", "Android");
        caps.setCapability("platformVersion", "6.0");
        caps.setCapability("skipUnlock","true");
        caps.setCapability("appPackage", "com.ticketmaster.mobile.android.na");
        caps.setCapability("appActivity","com.ticketmaster.mobile.android.library.activity.MainActivity");
        //caps.setCapability("appActivity", "com.ticketmaster.mobile.android.library.ui.activity.BranchActivity");
        caps.setCapability("noReset","true");
        driver = new AndroidDriver<MobileElement>(new URL("http://0.0.0.0:4723/wd/hub"),caps);
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void basicTest () throws InterruptedException {
        try {
          wait.until(ExpectedConditions.visibilityOfElementLocated
                  (By.xpath(hamburgerButtonXP))).click();

          wait.until(ExpectedConditions.visibilityOfElementLocated
                  (By.className("android.widget.TextView"))).click();

          wait.until(ExpectedConditions.visibilityOfElementLocated
                  (By.id("com.google.android.gms:id/cancel"))).click();

          wait.until(ExpectedConditions.visibilityOfElementLocated
                  (By.xpath(signUpButtonXP))).click();

          wait.until(ExpectedConditions.visibilityOfElementLocated
                  (By.id("com.google.android.gms:id/cancel"))).click();

          String firstName = "Joxex";
          String lastName = "Baxgixo";
          String email = "joxexl.bagioxt@gmail.com";
          String password = "n0m0r3p@!n";
          String zipCode = "23685";

          MobileElement fNel = driver.findElement(By.xpath(firstNameTBoxXP));
          fNel.sendKeys(firstName);
          wait.until(ExpectedConditions.textToBePresentInElement(fNel, firstName));

          MobileElement lNel = driver.findElement(By.xpath(lastNameTBoxXP));
          lNel.sendKeys(lastName);
          driver.hideKeyboard();
          wait.until(ExpectedConditions.textToBePresentInElement(lNel, lastName));

          MobileElement eNel = driver.findElement(By.xpath(emailTBoxXP));
          eNel.sendKeys(email);
          driver.hideKeyboard();

          wait.until(ExpectedConditions.textToBePresentInElement(eNel, email));
          driver.hideKeyboard();

          MobileElement pNel = (MobileElement) wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(passwordTBoxXP)));
          pNel.sendKeys(password);
          driver.hideKeyboard();

          wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(zipCodeTBoxXP)));
          MobileElement zNel = driver.findElement(By.xpath(zipCodeTBoxXP));
          zNel.sendKeys(zipCode);
          driver.hideKeyboard();

          wait.until(ExpectedConditions.elementToBeClickable(By.xpath(submitButtonXP))).click();
          Thread.sleep(10000);

          //driver.toggleAirplaneMode();
        }
        catch(TimeoutException e) {
          e.printStackTrace();
        }

    }

    public static void WaitMilliSeconds(int time){
      try {
        Thread.sleep(time);
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    @AfterMethod
    public void teardown(){
        driver.quit();
    }

}
